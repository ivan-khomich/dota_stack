var steam = require("steam"),
    util = require("util"),
    fs = require("fs"),
    dota2 = require("./dota2_client"),
    bot = new steam.SteamClient(),
    Dota2 = new dota2.Dota2Client(bot, true),
    express = require('express');

var redis = require("redis"),
    redisClient = redis.createClient();

global.config = require("./config");
var app = express();

 app.get('/', function(req, res){    
            res.send("OK");
 	});

/* Steam logic */

var onSteamLogOn = function onSteamLogOn(){
        bot.setPersonaState(steam.EPersonaState.Busy); // to display your bot's status as "Online"
        bot.setPersonaName(config.steam_name); // to change its nickname
        util.log("Logged on.");

        app.get('/send_invite/:steamid', function(req, res){
            bot.addFriend(req.params.steamid);
            console.log('send invite');            
            res.send("OK");
        });

       bot.on("friend", function(steamID, relationship) {
            if (relationship == steam.EFriendRelationship.PendingInvitee) {
                console.log("friend request received");
                steam.addFriend(steamID);
                console.log("friend request accepted");
            }
        });

        Dota2.launch();
        Dota2.on("ready", function() {
            console.log("Node-dota2 ready.");
            app.get('/send_party/:steamid', function(req, res){
                Dota2.inviteToPartyRequest(req.params.steamid, function(response){
                    console.log(err);
                    console.log(response);
                });   
                Dota2.on("partyUpdate", function(msg){
                    console.log(msg);
                    redisClient.set(msg.partyId, JSON.stringify(msg));
                    if(msg.members.length > 5){
                        Dota2.leaveParty();
                        redisClient.del(msg.partyId, function(err, numRemoved){
                            console.log(numRemoved);
                        });
                    }

                });
                res.send("OK");
            });
            app.get('/leave_party', function (req, res){
                setTimeout(function(){
                    Dota2.leaveParty();
                }, 45000);
                res.send("leaveParty")
            });
            app.get('/get_info/:steamid', function(req, res){
                Dota2.profileRequest(req.params.steamid, true, function(err, body){
                     res.send(JSON.stringify(body));
                });
            });

        });

        Dota2.on("unready", function onUnready(){
            console.log("Node-dota2 unready.");
        });

        Dota2.on("chatMessage", function(channel, personaName, message) {
            // util.log([channel, personaName, message].join(", "));
        });

        Dota2.on("guildInvite", function(guildId, guildName, inviter) {
        console.log("guildInvite");
             Dota2.setGuildAccountRole(guildId, 68430984, 3);
        });


        Dota2.on("unhandled", function(kMsg) {
            util.log("UNHANDLED MESSAGE " + kMsg);
        });
        // setTimeout(function(){ Dota2.exit(); }, 5000);
    },
    onSteamSentry = function onSteamSentry(sentry) {
        util.log("Received sentry.");
        require('fs').writeFileSync('sentry', sentry);
    },
    onSteamServers = function onSteamServers(servers) {
        util.log("Received servers.");
        fs.writeFile('servers', JSON.stringify(servers));
    },
    onWebSessionID = function onWebSessionID(webSessionID) {
        util.log("Received web session id.");
        // steamTrade.sessionID = webSessionID;
        bot.webLogOn(function onWebLogonSetTradeCookies(cookies) {
            util.log("Received cookies.");
            for (var i = 0; i < cookies.length; i++) {
                // steamTrade.setCookie(cookies[i]);
            }
        });
    };

// Login, only passing authCode if it exists
var logOnDetails = {
    "accountName": config.steam_user,
    "password": config.steam_pass
};
if (config.steam_guard_code) logOnDetails.authCode = config.steam_guard_code;
var sentry = fs.readFileSync('sentry');
if (sentry.length) logOnDetails.shaSentryfile = sentry;
bot.logOn(logOnDetails);
bot.on("loggedOn", onSteamLogOn)
    .on('sentry', onSteamSentry)
    .on('servers', onSteamServers)
    .on('webSessionID', onWebSessionID);

app.listen(4041);
