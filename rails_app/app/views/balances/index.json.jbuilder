json.array!(@balances) do |balance|
  json.extract! balance, :id, :port, :login, :password
  json.url balance_url(balance, format: :json)
end
