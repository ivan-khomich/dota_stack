json.array!(@users) do |user|
  json.extract! user, :id, :nickname, :name, :location, :avatar, :profile_url, :steamid
  json.url user_url(user, format: :json)
end
