#coding utf-8
class PartyController < ApplicationController
	before_action :empty_data_dota?

	def index
		#user = User.find(session[:current_user])
		@party = Party.all
		#render json: user.dota
	end

	def get_users
		@users = []
		p = Party.find(params[:id])
		p.users.each do | user |
			@users.push(User.find(user))
		end

		render json: @users
	end

	def join
		#RestClient.get("http://localhost:4040/send_party/#{user.steamid}")
		#flash[:message] = "Invite to party sent"

		#redirect_to action: :index
		all_users_in_party = []
		Party.where(["id != ?", params[:id]]).each do |party|
			all_users_in_party.concat(party.users)
		end
		
		if all_users_in_party.include?(session[:current_user])
			redirect_to :access_denied_for_party_exists
		else

			user = User.find(session[:current_user])
			request = RestClient.get("http://localhost:4040/get_info/#{user.steamid3}")
			user.dota = JSON.parse(request)
			user.save

			@party = Party.find(params[:id])
			unless @party.users.include? session[:current_user]
				@party.users.push(session[:current_user])
				@party.save
			end
		end
		#if @party.users.length > 5
		#	@party.users.each do | user |
		#		RestClient.get("http://localhost:4040/send_party/#{User.find(user).steamid}")
		#	end
		#	@party.destroy
		#end
		
	end

	def send_invite_to_party
		@party = Party.find(params[:id])
		balance =  Balance.where(["updated_at < ?", 46.second.ago]).sample
		unless balance.nil?
			if @party.create_user == session[:current_user]
				@party.users.each do | user |
					RestClient.get("http://localhost:#{balance.port}/send_party/#{User.find(user).steamid}")
				end
				#@party.delete
				#@party.save
			end
			RestClient.get("http://localhost:#{balance.port}/leave_party")
		end

		begin
		  @party.delete
		  @party.save
		rescue Exception => e

		end

		redirect_to :invite_sent_page
	end

	def create
		all_users_in_party = []
		Party.all.each do |party|
			all_users_in_party.concat(party.users)
		end
		
		if all_users_in_party.include?(session[:current_user])
			redirect_to :access_denied_for_party_exists
		else
			p = Party.where(["create_user = ?", session[:current_user]])
			if p.empty?
				p = Party.new
				p.create_user = session[:current_user]
				p.users = [session[:current_user]]
				p.save

				redirect_to "/join_party/#{p.id}"
			else
				redirect_to action: :index
			end
		end
	end

	def leave
		party = Party.where(id: params[:id]).first
		unless party.nil?
			unless params[:user_id].nil?
				begin 
					if party.create_user == session[:current_user]
						if party.users.include?(params[:user_id].to_i)
							party.users.delete(params[:user_id].to_i)
							party.save
						end
					end
				rescue Exception => e

				end
				redirect_to :back
			else
				begin 
					if party.users.include?(session[:current_user])
						party.users.delete(session[:current_user])
						if party.create_user == session[:current_user]
							party.delete
						end
						party.save
					end
				rescue Exception => e

				end
				redirect_to '/party'
			end
		else
			redirect_to '/party'
		end
	end

	private

	def empty_data_dota?
		user = User.find(session[:current_user])
		redirect_to :access_denied_for_party if user.dota.blank?
	end

end
