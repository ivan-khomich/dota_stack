class ChatController < WebsocketRails::BaseController

  
  def incoming_message
  	user = User.find(session[:current_user])
	broadcast_message :new_message, {:user => user["dota"]["playerName"], :message => message}
  end

  def client_subscribed
  	user = User.find(session[:current_user])
  	WebsocketRails[message[:channel]].trigger(:new_message_channel, {:user => user["dota"]["playerName"], :message => message})
  	p message
  end

  def client_connected
  	p "!!" * 40
  end
end