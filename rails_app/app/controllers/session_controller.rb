class SessionController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def create
  	#render :text => auth.to_json
    user = User.where(["steamid = ?", auth[:uid]]).first
    if user.nil?
    	user 				= User.new
    	user.steamid  		= auth[:uid]
      user.steamid3     = convert_stem_id(auth[:uid])
    	user.nickname 		= auth[:info][:nickname]
    	user.name 	  		= auth[:info][:name]
    	user.location 		= auth[:info][:location]
    	user.avatar	  		= auth[:info][:image]
    	user.profile_url 	= auth[:info][:urls][:Profile]
    	user.save
    end

    session[:current_user] = user.id

    #self.current_user = @user
    redirect_to '/'
  end

  def out
    session[:current_user] = nil
    redirect_to '/'
  end

  protected

  def auth
    request.env['omniauth.auth']
  end

  def request_invite(steamid)
    RestClient.get 'http://localhost:4040/send_invite/'+steamid
  end

  def convert_stem_id(steamid)
    #if (strlen($id) === 17)
    #{
    #    $converted = substr($id, 3) - 61197960265728;
    #}
    #else
    #{
    #    $converted = '765'.($id + 61197960265728);
    #}
    if steamid.length === 17
      return steamid[3..steamid.length].to_i - 61197960265728
    else
      return "765" + (steamid.to_i + 61197960265728)
    end
  end
end
