class MainController < ApplicationController
	def index
		$redis = Redis.new db: 0, driver: :hiredis
		@count_users = 0
		$redis.keys.each do | key |
			p key
			if key.length == 17
				p key
				object = JSON.parse($redis.get(key))
				object.each do | v |
					puts v
				end
				unless object["memberIds"].nil?
					p object["memberIds"]
					@count_users = object["memberIds"].length
				end
			end
		end
		unless session[:current_user].nil?
			@user = User.where(["id = ? ", session[:current_user] ]).first
			if @user.nil?
				session[:current_user] = nil
				redirect_to '/'
			end

		end
	end

	#def join_party
	#	user = User.find(session[:current_user])
	#	RestClient.get("http://localhost:4040/send_party/#{user.steamid}")
	#	flash[:message] = "Invite to party sent"
	#	redirect_to action: :index
	#end

	def send_invite
		user = User.find(session[:current_user])
		RestClient.get("http://localhost:4040/send_invite/#{user.steamid}")
		flash[:message] = "Invite sent"
		redirect_to action: :index
	end

	def access_denied_for_party
		#REUQEST DATA
		user = User.find(session[:current_user])
		request = RestClient.get("http://localhost:4040/get_info/#{user.steamid3}")
		user.dota = JSON.parse(request)
		user.save
	end

	def access_denied_for_party_exists
		Party.all.each do | party |
			if party.users.include?(session[:current_user])
				@party = party
				break
			end
		end
	end

	def invite_sent

	end

end
