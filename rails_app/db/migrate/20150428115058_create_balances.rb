class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.integer :port
      t.string :login
      t.string :password

      t.timestamps
    end
  end
end
