class CreateParties < ActiveRecord::Migration
  def change
    create_table :parties do |t|
      t.text :users
      t.integer :create_user

      t.timestamps
    end
  end
end
