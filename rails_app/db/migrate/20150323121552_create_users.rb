class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nickname
      t.string :name
      t.string :location
      t.string :avatar
      t.string :profile_url
      t.string :steamid

      t.timestamps
    end
  end
end
